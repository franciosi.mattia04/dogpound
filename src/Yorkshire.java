/**
 Yorkshire.java
 A class derived from Dog that holds information about
 a Yorkshire terrier. Overrides Dog speak method.
@ TODO this file is largely incomplete

 */

public class Yorkshire extends Dog
{


    public Yorkshire(String name)
    {
        super(name);
    }

    /**
     * Small bark -- overrides speak method in Dog
     * @return a small bask string
     */

    public String speak()
    {
        return "woof";
    }
    public int avgBreedWeight(){
        return 75;
    }
}